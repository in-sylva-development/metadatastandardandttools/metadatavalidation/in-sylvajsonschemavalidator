import os

class Config:
    SECRET_KEY = 'ton_secret_key'
    BASE_DIR = 'data'
    UPLOAD_FOLDER = 'uploads'
    RESULTS_FOLDER = 'results'
    SCHEMA = 'validator/IN-SYLVA_schema_2024.json'
    VALIDATOR_FOLDER = 'validator'
    VALIDATOR_SCRIPT = 'validationPython.py'
    INPUT_JSON_FILE_NAME = 'input.json'    
    TIME_OUT = '1' #durée pour expiration de la session (en heure)
    
    # Préfixe de route pour que l'application soit accessible avec une URL préfixée (http://localhost:5000/jschecker/ par exmeple)
    # Si la variable d'environnement BASE_URL n'est définie, la valeur par défaut est une chaîne vide.
    BASE_URL = os.environ.get('BASE_URL', '')  
    FILE_METADATA = {}
    ANALYSIS_STATUS = {}
 
class DevelopmentConfig(Config):
    DEBUG = True

 
config = {
    'development': DevelopmentConfig,
}