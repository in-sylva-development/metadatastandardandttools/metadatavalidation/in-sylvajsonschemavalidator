def compter_erreurs(fichier_chemin):
    try:
        # Ouvre le fichier en mode lecture
        with open(fichier_chemin, 'r') as fichier:
            contenu = fichier.read()
        
        # Compter le nombre de fois que "NEXT ERROR" apparaît
        nombre_erreurs = contenu.count("NEXT ERROR")
        
        # print(f"Nombre d'erreur: {nombre_erreurs}")
        return nombre_erreurs
    except FileNotFoundError:
        print(f"Erreur : Le fichier {fichier_chemin} n'a pas été trouvé.")
        return -1
    except Exception as e:
        print(f"Erreur lors de l'analyse du fichier : {e}")
        return -1
