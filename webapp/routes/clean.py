from flask import Blueprint, session, redirect, url_for, flash, current_app, render_template
import os
 
clean_bp = Blueprint('clean', __name__)

@clean_bp.route('/clean', methods=['POST'])
def clean_files():
    analysis_status = current_app.config['ANALYSIS_STATUS']
    file_metadata = current_app.config['FILE_METADATA']

    file_id = session.get('file_id')

    if not file_id:
        flash("Aucun fichier à nettoyer.", "error")
        return redirect(url_for('upload.index'))
   
    try:
        json_file = os.path.join(session['upload_folder'], file_id)
        if os.path.exists(json_file):
            os.remove(json_file)
        else:
            print("Le fichier n'existe pas")
       
        result_files = [ file['name'] for file in file_metadata[file_id]['files']]

        for file in result_files:
            if os.path.exists(file):
                os.remove(file)
       
        del analysis_status[file_id]
        del file_metadata[file_id]
        session.pop('file_id', None)
        flash("Fichiers nettoyés avec succès.", "success")

    except Exception as e:
        flash(f"Erreur lors du nettoyage : {e}", "error")
   
    return redirect(url_for('upload.index'))