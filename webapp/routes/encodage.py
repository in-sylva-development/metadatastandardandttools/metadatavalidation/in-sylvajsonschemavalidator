from flask import Blueprint
from flask import request, jsonify
import cchardet as chardet

encodage_bp = Blueprint('encodage', __name__)

@encodage_bp.route('/analyze_encoding', methods=['POST'])
def analyze_encoding():
    file = request.files.get('file')
    if not file:
        return jsonify({"error": "No file provided"}), 400
    
    file_data = file.read()  # Lire le contenu du fichier
    encoding_detection = chardet.detect(file_data)
    
    confidence = encoding_detection['confidence']
    encoding = encoding_detection['encoding']
    
    # Retourner l'encodage et l'indice de confiance au frontend
    return jsonify({"encoding": encoding, "confidence": confidence})

