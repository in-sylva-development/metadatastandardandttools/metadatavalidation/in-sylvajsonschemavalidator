from flask import Blueprint, session, flash, redirect, session, url_for, current_app, render_template, send_file
import os, io, zipfile

files_bp = Blueprint('files', __name__)
@files_bp.route('/files')
def list_files():
    analysis_status = current_app.config['ANALYSIS_STATUS']
    file_metadata = current_app.config['FILE_METADATA']

    file_id = session.get('file_id')
    if not file_id or analysis_status.get(file_id) != 'completed':
        flash("L'analyse du fichier est en cours ou a échoué.", "error")
        return redirect(url_for('upload.index'))
   
    files_info = file_metadata.get(file_id, {}).get('files', [])
    return render_template('files.html', files_info=files_info, file_id=file_id, BASE_URL=os.getenv('BASE_URL', 'http://localhost:5000'))


@files_bp.route('/results/<path:filename>')
def view_file(filename):
    analysis_status = current_app.config['ANALYSIS_STATUS']
    print(f"fichier à visualiser: {filename}")
    file_id = current_app.config['INPUT_JSON_FILE_NAME']
    if not file_id or analysis_status.get(file_id) != 'completed':
        flash("L'accès à ce fichier est interdit.", "error")
        return redirect(url_for('files.list_files'))
    
    result_file_path = filename

    if os.path.getsize(result_file_path) == 0:
        flash("Ce fichier est vide et ne peut pas être affiché.", "warning")
        return redirect(url_for('files.list_files'))
    try:
        with open(result_file_path, 'r') as file:
            content = file.read()
    except FileNotFoundError:
        flash("Fichier non trouvé", "error")
        return redirect(url_for('files.list_files'))
 
    return render_template('view_file.html', filename=filename, content=content, BASE_URL=os.getenv('BASE_URL', 'http://localhost:5000'))


@files_bp.route('/download')
def download_archive():
    file_id = session.get('file_id')
    file_metadata = current_app.config['FILE_METADATA']

    if not file_id or not file_metadata.get(file_id):
        flash("Aucun fichier trouvé pour l'archive.", "error")
        return redirect(url_for('files.list_files'))
    
    # Création de l'archive ZIP dans la mémoire
    archive_buffer = io.BytesIO()
    with zipfile.ZipFile(archive_buffer, 'w', zipfile.ZIP_DEFLATED) as zip_archive:
        for file_info in file_metadata[file_id]['files']:
            file_path = file_info['name']
            print(file_path)
            if os.path.exists(file_path) :
                if os.path.getsize(file_path) > 0 : #& os.stat(file_path).st_size > 0 :
                    zip_archive.write(file_path, arcname=os.path.basename(file_path))
                #else :
                    #print(f"{file_path} : fichier vide.")
            #else:
                #print(f"{file_path} : n'existe pas.")
    
    archive_buffer.seek(0)  # Revenir au début du fichier pour le télécharger
    
    return send_file(archive_buffer, as_attachment=True, download_name=f"{file_id}_results.zip")
