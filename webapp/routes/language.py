from flask import Blueprint, session, redirect, url_for

language_bp= Blueprint('language', __name__)

@language_bp.route('/change_language/<lang>')
def change_language(lang):
    session['lang'] = lang
    return(redirect(url_for('upload.index')))