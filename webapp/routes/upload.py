from flask import Blueprint, flash, request, redirect, url_for, session, current_app, render_template
import os
import subprocess
import re
from routes.auth import login_required


from tools import compter_erreurs

# Fonction utilisée pour trier les résultats sur l'ordre numérique plutôt que l'ordre aphabétique
def extract_number(filename):
    # \d+    : permet de trouver tout ce qui ressemble à un nombre
    match = re.search(r'\d+', filename)
    return int(match.group()) if match else float('inf')

upload_bp = Blueprint('upload', __name__)

@upload_bp.route('/', methods=['GET'])
@login_required
def index():
    schema = current_app.config.get('SCHEMA', 'Valeur par défaut')
    return render_template('index.html', show_language_buttons=True, BASE_URL=os.getenv('BASE_URL', 'http://localhost:5000'),schema=schema)


 
@upload_bp.route('/upload', methods=['POST'])
@login_required
def upload_file():
    messages = session.get('messages', {})
    analysis_status = current_app.config['ANALYSIS_STATUS']
    file_metadata = current_app.config['FILE_METADATA']

    file = request.files.get('file')

    MAX_FILE_SIZE = 50 * 1024 * 1024  # 50 Mo en octets

    file = request.files.get('file')
    if not file or file.filename == '':
        flash(messages["flash"]["file_is_missing"], "error")
        return redirect(url_for('upload.index'))

    # Vérifier la taille du fichier
    file.seek(0, os.SEEK_END)  # Déplace le curseur à la fin du fichier pour obtenir la taille
    file_size = file.tell()
    file.seek(0)  # Remet le curseur au début pour permettre l'enregistrement

    if file_size > MAX_FILE_SIZE:
        flash(messages["flash"]["file_size_max"], "error")
        return redirect(url_for('upload.index'))

    if not file or file.filename == '':
        flash(messages["flash"]["file_is_missing"], "error")
        return redirect(url_for('upload.index'))

    file_id = current_app.config['INPUT_JSON_FILE_NAME']
    file_path = os.path.join(session['upload_folder'], file_id)
    file.save(file_path)
    result_path = os.path.join(session['results_folder'])
    result_file = os.path.join(result_path,"output_result.txt")
    vocab_control = 'vocab_control' in request.form
    simple_output = 'simple_output' in request.form
    synthesis = 'synthesis_output' in request.form
    mask_output = 'mask_output' in request.form
    hide_empty = 'hide_empty' in request.form

    # Récupérer l'encodage depuis le champs caché
    file_encoding = request.form.get('file_encoding')
    
    command = f"python3 {current_app.config['VALIDATOR_FOLDER']}/{current_app.config['VALIDATOR_SCRIPT']} {current_app.config['SCHEMA']} {file_path} {session['results_folder']} -e {file_encoding}"
    print(command)

    if vocab_control:
        command += " --cv"
    if simple_output:
        command += " --ss"
    if synthesis:
        command += " -s"
    command += f" > {result_file}"
    analysis_status[file_id] = 'in_progress'
    file_metadata[file_id] = {'hide_empty': hide_empty, 'files': []}

    try:
        result = subprocess.run(command, shell=True, check=True)

        if result.returncode == 0:
            analysis_status[file_id] = 'completed'
            print(f"L'analyse du fichier {file_id} s'est terminée avec succès.")
        else:
            error_log_path=os.path.join(session['results_folder'],"error_log.txt")
            with open(error_log_path, "r") as error_file:
                error_message = error_file.read()
            analysis_status[file_id] = 'error'
            print(f"Erreur: L'analyse du fichier {file_id} a échoué avec le code de retour {result.returncode}")
            # stockage dans la variable de session
            session['error_info'] = {
                'file_name': file.filename,
                'encoding': file_encoding,
                'error_message': error_message
            }
            flash(messages["flash"]["file_json_reading_error"], "error")
            return redirect(url_for('upload.show_json_error', file_id=file_id))


        
        sorted_files = sorted(os.listdir(result_path), key=extract_number) # tri des fichiers en prenant en compte les valeurs numériques
        for file_name in sorted_files:
            file_path = os.path.join(result_path, file_name)

            if os.path.isfile(file_path):
                file_size = os.path.getsize(file_path)

                # Si le fichier est vide et que l'option hide_empty est activée, on le supprime
                if file_size == 0 and hide_empty:
                    os.remove(file_path)
                elif  "output" in file_name and mask_output:
                    os.remove(file_path) 
                else:
                    # Si le fichier n'est pas vide (ou si hide_empty n'est pas activé), on l'ajoute
                    if file_id not in file_metadata:
                        file_metadata[file_id] = {'files': []}
                    nb_erreurs = compter_erreurs(file_path)
                    file_metadata[file_id]['files'].append({'name': file_path, 'size': file_size, 'errors': nb_erreurs})


    except subprocess.CalledProcessError as e:
        analysis_status[file_id] = 'failed'
         # Stocker les informations d'erreur dans la session
        session['error_info'] = {
            'file_name': file.filename,
            'encoding': file_encoding,
            'error_message': str(e)
            }

        # Vérifie si la sortie d'erreur est disponible
        if e.stderr:
            session['error_info']['stderr'] = e.stderr.strip()  # Capture la sortie d'erreur
        else:
            session['error_info']['stderr'] = messages["error"]["nostderr"]
        # Vérifie si la sortie standard est disponible
        if e.stdout:
            session['error_info']['stdout'] = e.stdout.strip()  # Capture la sortie standard
        else:
            session['error_info']['stdout'] = messages["error"]["nostdout"]

        flash(f"Erreur lors de l'exécution du script : {e}", "error")
        return redirect(url_for('upload.show_error', file_id=file_id))
   
    session['file_id'] = file_id
    flash(messages["flash"]["success_analyze"], "success")
    return redirect(url_for('files.list_files'))

@upload_bp.route('/upload/error/json/<file_id>')
def show_json_error(file_id):
    error_info = session.get('error_info', {})
    if not error_info:
        flash(messages["flash"]["no_error_message"], "error")
        return redirect(url_for('upload.index'))

    return render_template('json_read_error.html', error_info=error_info)


@upload_bp.route('/error/<file_id>')
def show_error(file_id):
    # Récupération des informations d'analyse depuis un stockage (par exemple `analysis_status`)
    file_info = session.get('error_info', {})
    file_name = file_info.get("file_name", "Inconnu")
    encoding = file_info.get("encoding", "Inconnu")
    error_message = file_info.get("error_message", "Erreur non spécifiée")
    stderr = file_info.get("stderr", "")
    stdout = file_info.get("stdout", "")
    error_log_path=os.path.join(session['results_folder'],"error_log.txt")
    try:
        with open(error_log_path, "r") as error_file:
            error_content = error_file.read()
    except:
            error_content = "Erreur de lecture du json, mais pas de fichier d'erreur trouvé...Vérifiez le fichier json fourni"

    return render_template('error.html', file_name=file_name, encoding=encoding, error_message=error_message, stderr=stderr, stdout=stdout, error_content=error_content, BASE_URL=os.getenv('BASE_URL', 'http://localhost:5000'))

