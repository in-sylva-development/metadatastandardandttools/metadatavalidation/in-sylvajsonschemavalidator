from flask import Blueprint, render_template, request, redirect, url_for, session, flash
from functools import wraps
import os

auth_bp = Blueprint('auth', __name__)

# Identifiant et mot de passe fixes
USERNAME = 'in-sylva-user'
PASSWORD = 'check2024'

# Vérification de la variable d'environnement
DISABLE_AUTH = os.environ.get('DISABLE_AUTHENTICATION') is not None

def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if DISABLE_AUTH:
            # Si l'authentification est désactivée, passer directement à la route
            return f(*args, **kwargs)

        if 'logged_in' not in session:
            return redirect(url_for('auth.login'))
        session.permanent = True  # Activer le mode permanent pour l'expiration de session
        return f(*args, **kwargs)
    return decorated_function

@auth_bp.route('/login', methods=['GET', 'POST'])
def login():
    if DISABLE_AUTH:
        # Si l'authentification est désactivée, passer directement à l'index
        session['logged_in'] = True
        session.permanent = True
        flash('Authentification désactivée. Vous êtes connecté.', 'info')
        return redirect(url_for('upload.index'))

    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']

        if username == USERNAME and password == PASSWORD:
            session['logged_in'] = True
            session.permanent = True
            return redirect(url_for('upload.index'))
        else:
            flash('Identifiant ou mot de passe incorrect', 'error')

    return render_template('login.html', BASE_URL=os.getenv('BASE_URL', 'http://localhost:5000'))

@auth_bp.route('/logout')
def logout():
    if DISABLE_AUTH:
        # Si l'authentification est désactivée, rediriger directement
        flash('L\'authentification est désactivée, aucune déconnexion nécessaire.', 'info')
        return redirect(url_for('upload.index'))

    session.pop('logged_in', None)
    flash('Vous avez été déconnecté', 'success')
    return redirect(url_for('auth.login'))
