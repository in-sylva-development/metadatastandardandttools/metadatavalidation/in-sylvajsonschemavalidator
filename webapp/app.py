from flask import Flask, session, render_template
from routes.upload import upload_bp
from routes.files import files_bp
from routes.clean import clean_bp
from routes.language import language_bp
from routes.encodage import encodage_bp
from routes.auth import auth_bp

from datetime import timedelta

import json, uuid, os
from config import config as app_config
    
app = Flask(__name__, static_url_path=os.getenv('BASE_URL', '') + '/static')

app.config.from_object(app_config['development'])

# Configurer la durée d'expiration de session
app.permanent_session_lifetime = timedelta(hours=int(app.config.get('TIME_OUT')))

root_url = app.config.get('BASE_URL', '')

os.makedirs(app.config['UPLOAD_FOLDER'], exist_ok=True)
os.makedirs(app.config['RESULTS_FOLDER'], exist_ok=True)

# Enregistrer les blueprints
app.register_blueprint(upload_bp, url_prefix=root_url)
app.register_blueprint(files_bp, url_prefix=root_url)
app.register_blueprint(clean_bp, url_prefix=root_url)
app.register_blueprint(language_bp, url_prefix=root_url)
app.register_blueprint(encodage_bp,url_prefix=root_url)
app.register_blueprint(auth_bp,url_prefix=root_url)

print(f"Running on http://127.0.0.1:5000{root_url}")
# Charger les traductions en fonction de la langue choisie
def load_translations(lang='fr'):
    try:
        with open(os.path.join('webapp/translations', f'messages_{lang}.json'), 'r', encoding='utf-8') as file:
            messages = json.load(file)
        return messages
    except FileNotFoundError:
        return {}
 
# Injecter la langue actuelle et les messages dans les templates
@app.context_processor
def inject_translations():
    current_lang = session.get('lang', 'fr')
    messages = load_translations(current_lang)
    session['messages'] = messages  # Stocker les messages dans la session
    return dict(messages=messages, current_lang=current_lang)

@app.before_request
def create_session_directory():
    # Si la session n'a pas encore d'identifiant unique, on en génère un, et on créé les répertoires
    if 'session_id' not in session:
        session['session_id'] = str(uuid.uuid4())  # Génère un identifiant unique
        upload_directory = os.path.join(app.config['BASE_DIR'],session['session_id'],app.config['UPLOAD_FOLDER'])
        results_directory = os.path.join(app.config['BASE_DIR'],session['session_id'],app.config['RESULTS_FOLDER'])
        os.makedirs(upload_directory, exist_ok=True)
        os.makedirs(results_directory, exist_ok=True)
        print(f"Répertoires de session créé : {upload_directory}, {results_directory}")
        session['upload_folder'] = upload_directory
        session['results_folder'] = results_directory
    else: 
        upload_directory = os.path.join(app.config['BASE_DIR'],session['session_id'],app.config['UPLOAD_FOLDER'])
        results_directory = os.path.join(app.config['BASE_DIR'],session['session_id'],app.config['RESULTS_FOLDER'])
        if not os.path.isdir(upload_directory):
            os.makedirs(upload_directory, exist_ok=True)
        if not os.path.isdir(results_directory):
            os.makedirs(results_directory, exist_ok=True)

# Définition d'un filtre personnalisé pour basename (utilisation dans le template view_file.html)
@app.template_filter('basename')
def basename_filter(path):
    return os.path.basename(path)

@app.route(f'{root_url}/aide')
def aide():
    return render_template('aide.html', BASE_URL=os.getenv('BASE_URL', 'http://localhost:5000')
)

@app.route('/favicon.ico')
def favicon():
    return '', 204

if __name__ == '__main__':
    app.run(debug=True)

