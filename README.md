# In-SylvaJSONSchemaValidator

## Why use it
This python package should be used if you want to check json file conformity according to a provided schema.

It has been created for the **In-Sylva France project**, but one can change the schema file and therefore check the conformity for json files with another schema

## Authors

This project was developed by:

- **[Christian Pichot]** (<christian.pichot@inrae.fr>) - [developper/ script validationPython.py]
- **[Philippe Clastre]** (<philippe.clastre@inrae.fr>) - [developper/ flask webapp, docker]

For more details and a full history of contributions, please check the repository's history.

## How to use it
First of all, you have to clone the repository:
```bash
git clone git@forgemia.inra.fr:in-sylva-development/metadatastandardandttools/metadatavalidation/in-sylvajsonschemavalidator.git
```

This will create a directory named *in-sylvajsonschemavalidator*.

There are three usage modes
* *command line*: use only the validator  
* *flask webapp*: use the validator thru a web page hosted by a local 
* *dockerized webapp*: use the validator thru a web page hosted in a docker container

## ------ Command line usage -----
This command is provided in french language only
usage: validationPython.py [-h] [-cv] [-ss] [-s] [-e ENCODING]
                           schema input_json output_directory

Script de validation d'un fichier JSON par rapport à un schéma JSON

positional arguments:
  schema                Chemin vers le schéma JSON
  input_json            Chemin vers le fichier JSON à controler
  output_directory      Chemin vers le répertoire de sortie

optional arguments:
  -h, --help            show this help message and exit
  -cv, --cv, --controle-vocabulaire
                        Active le contrôle du vocabulaire
  -ss, --ss, --sortie-simplifiee
                        Active la généreration d'une sortie simplifiée
  -s, --s, --synthese   Active la génération du fichier de synthèse
  -e ENCODING, --encoding ENCODING
                        Spécifie l'encodage du fichier JSON (par défaut:
                        utf-8)

### Example
```bash
cd in-sylvajsonschemavalidator/validator
python3 validationPython.py IN-SYLVA_schema_2023.json ../schema_and_metadata/myJSON_MetadataRecord.json ../schema_and_metadata
```

### Input
Schema and metadata record to validate are in the schema_and_metadata directory

### Output
A directory is created in the input dir. 

It will contains as many files as the number of records in the json input file.
These files are names: record_[n].txt where n is the record number

If specified, you would also find a simplified output for each record

If specified, you would also find a summarize validation errors 

## ------ Flask webapp usage -----
In this mode, the web interface help the user to specify what file should be checked.

Note: by default, the schema validation is validator/IN-SYLVA_schema_2023.json. This file is defined in the configuration file of the webapp (webapp/config.py)

To start the webapp:
```bash

cd in-sylvajsonschemavalidator
python3 webapp/app.py
```

Then, go the page URL: http://localhost:5000

### Prefix URL
If you set BASE_URL environment variable to "/my_app" value, then the application will be accessible to *http://localhost:5000/my_app*

*Important* Do not forget the leading "/" in the base_url you provide.

### Disable authentication
If you want to run this application without authentication, you just have to set an environment variable *DISABLE_AUTHENTICATION* before launching the application.

When used in container mode, the variable must be passed the same way the BASE_URL variable is. (see below)

#### Exemple 
In this situation, the application is launched without authentication, and on a default localhost adress

```bash
export DISABLE_AUTHENTICATION=1
python3 webapp/app.py
```

### Zip output
The webapp version provided also a way to download the conformity result.

It is provided as a zip file.

You can download it and check each record status by looking insied each record output result.

## ------ Dockerized Flask webapp usage -----
In this mode, the web interface help the user to specify what file should be checked.

Note: by default, the schema validation is validator/IN-SYLVA_schema_2023.json. This file is defined in the configuration file of the webapp (webapp/config.py)

To start the webapp:
```bash

cd in-sylvajsonschemavalidator
# generate the docker image on your machine
docker build -t in-sylva-schema-validator .
# launch the webapp in a docker container
docker run -d -p 5000:5000 --rm --name validator in-sylva-schema-validator 
```

Then, go the page URL: http://localhost:5000

### Prefix URL
If you want to access the wepapp with an URL like http://localhost:5000/jsonChecker , then you should launch this command:

```bash
export BASE_URL="/jsonChecker"
docker run -d -p 5000:5000 -e BASE_URL=$BASE_URL --rm --name validator in-sylva-schema-validator 
```
### Zip output
The webapp version provided also a way to download the conformity result.

It is provided as a zip file.

You can download it and check each record status by looking insied each record output result.

### Deleting output
when the user visits the page with files listing, he can delete all output with the corresponding button.

This is a good practice, otherwise, all files remains on the file system

**You should be aware that clicking on this button will delete outputs and redirect to the upload page**

## Trouble

### jsonschema version
If this error is shown:
```
Traceback (most recent call last):
  File "validationPython.py", line 152, in <module>
    validator = jsonschema.Draft7Validator(schema, format_checker=FormatChecker())
AttributeError: module 'jsonschema' has no attribute 'Draft7Validator'
```
You can solve the problem by upgrading the jsonschema module
```bash
pip3 install jsonschema --upgrade
```

### Encoding trouble
The library used to load the json is very sensible to encoding

The recommendation is:*try to ensure that your json file is fully encoded in UTF-8, which is must suited to better user experience !*