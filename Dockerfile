
FROM python:3.9-slim
WORKDIR /app

RUN apt update && apt install -yy curl

COPY requirements.txt requirements.txt

RUN pip install --no-cache-dir -r requirements.txt

COPY . .

EXPOSE 5000

ENV FLASK_APP=run.py
ENV FLASK_ENV=production
ENV PYTHONPATH=/app/webapp

CMD ["gunicorn", "--bind", "0.0.0.0:5000", "webapp.app:app", "docker"]
