import os
import sys
from pathlib import Path
import json
import jsonschema
from jsonschema import validate
from jsonschema import FormatChecker
from jsonschema.exceptions import ErrorTree
import urllib
import time
import ssl
import output_simplifier as output_simplifier
import argparse

ssl._create_default_https_context = ssl._create_stdlib_context
#ssl._create_default_https_context = ssl._create_unverified_context
#export PYTHONHTTPSVERIFY=0

# usage
print("********************** usage ********************************************************")
print("  python3 validationPython.py schemaFile jsonFile outputDir {--cv --s  --S} ")
print("*************************************************************************************")

# controles des arguments
parser = argparse.ArgumentParser(description="Script de validation d'un fichier JSON par rapport à un schéma JSON")

# Ajout des arguments obligatoires
parser.add_argument("schema", type=str, help="Chemin vers le schéma JSON")
parser.add_argument("input_json", type=str, help="Chemin vers le fichier JSON à controler")
parser.add_argument("output_directory", type=str, help="Chemin vers le répertoire de sortie")

# Ajout des arguments optionnels (booléens) avec des valeurs par défaut
parser.add_argument("-cv", "--cv","--controle-vocabulaire", action="store_true", help="Active le contrôle du vocabulaire")
parser.add_argument("-ss", "--ss","--sortie-simplifiee", action="store_true", help="Active la généreration d'une sortie simplifiée")
parser.add_argument("-s", "--s","--synthese", action="store_true", help="Active la génération du fichier de synthèse")
parser.add_argument("-e", "--encoding", type=str, default="utf-8", help="Spécifie l'encodage du fichier JSON (par défaut: utf-8)")

# Parsing des arguments
args = parser.parse_args()
json_encoding = args.encoding

#load schema
#sche = open('/home/cpichot/Documents/Projets/France/IR/IN_SYLVA/realisation/SI/Portail/InitialisationPortail/2024/Schema/IN-SYLVA_schema_2023.json')
sche = open(args.schema)
schema = json.load(sche)

#paths for controlled Vocabularies
controlled_Voc = {'resource.date.type' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/resource.date.type.json',
'resource.IN-SYLVA_type' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/resource.IN-SYLVA_type.json',
'resource.INSPIRE_type' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/resource.INSPIRE_type.json',
'resource.language' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/resource.language.json',
'resource.IN-SYLVA_theme' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/resource.IN-SYLVA_theme.json',
'context.forest_type' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/context.forest_type.json',
'context.reference_soil_group' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/context.reference_soil_group.json',
'context.humus_type' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/context.humus_type.json',
'context.related_experimental_network_title' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/context.related_experimental_network_title.json',
'context.studied_compartment' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/context.studied_compartment.json',
'responsible_organisation.name' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/responsible_organisation.name.json',
'responsible_organisation.unit_name' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/responsible_organisation.unit_name.json',
'keyword.controlled_vocabulary.date.type' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/keyword.controlled_vocabulary.date.type.json',
'studied_factor.name' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/studied_factor.name.json',
'studied_factor.comparison_modality' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/studied_factor.comparison_modality.json',
'biological_material.genetic_level' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/biological_material.genetic_level.json',
'biological_material.taxonomy_vocabulary.date.type' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/biological_material.taxonomy_vocabulary.date.type.json',
'variable.observable_property' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/variable.observable_property.json',
'variable.object_of_interest' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/variable.object_of_interest.json',
'variable.data.acquisition_mode' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/variable.data.acquisition_mode.json',
'variable.data.information_granularity' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/variable.data.information_granularity.json',
'experimental_site.stand_structure' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/experimental_site.stand_structure.json',
'experimental_site.experiment_status' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/experimental_site.experiment_status.json',
'experimental_site.experiment_design' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/experimental_site.experiment_design.json',
'experimental_site.other_information_available' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/experimental_site.other_information_available.json',
'metadata.language' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/metadata.language.json',
'metadata.responsible_organisation.name' : 'https://w3.avignon.inra.fr/gn_plateau_ressources/INSYLVA/metadata.responsible_organisation.name.json'}



#load controlled Vocabularies
if args.cv :
    print("Vocabularies will be checked")

    for voc in controlled_Voc:
        with urllib.request.urlopen(controlled_Voc[voc]) as f:
            jsonVoc = json.loads(f.read())
        def get_val(key):
            return [entry["pref_label_en"][key] for entry in jsonVoc["result"]["sparql"]["results"]["bindings"]]
        vocList = get_val("value")
        controlled_Voc[voc] = vocList #replace path by the list of values

    #assign Vocabularies in the schema
    #schema["properties"]["resource"]["properties"]["date"]["items"]["anyOf"][0]["properties"]["type"]["enum"] = controlled_Voc['resource.date.type']
    schema["properties"]["resource"]["properties"]["date"]["items"]["properties"]["type"]["enum"] = controlled_Voc['resource.date.type']
    schema["properties"]["resource"]["properties"]["IN-SYLVA_type"]["enum"] = controlled_Voc['resource.IN-SYLVA_type']
    schema["properties"]["resource"]["properties"]["INSPIRE_type"]["enum"] = controlled_Voc['resource.INSPIRE_type']
    #schema["properties"]["resource"]["properties"]["language"]["items"]["anyOf"][0]["enum"] = controlled_Voc['resource.language']
    schema["properties"]["resource"]["properties"]["language"]["items"]["enum"] = controlled_Voc['resource.language']
    #schema["properties"]["resource"]["properties"]["IN-SYLVA_theme"]["items"]["anyOf"][0]["enum"] = controlled_Voc['resource.IN-SYLVA_theme']
    schema["properties"]["resource"]["properties"]["IN-SYLVA_theme"]["items"]["enum"] = controlled_Voc['resource.IN-SYLVA_theme']
    schema["properties"]["context"]["properties"]["forest_type"]["enum"] = controlled_Voc['context.forest_type']
    schema["properties"]["context"]["properties"]["reference_soil_group"]["enum"] = controlled_Voc['context.reference_soil_group']
    schema["properties"]["context"]["properties"]["humus_type"]["enum"] = controlled_Voc['context.humus_type']
    schema["properties"]["context"]["properties"]["related_experimental_network_title"]["enum"] = controlled_Voc['context.related_experimental_network_title']
    #schema["properties"]["context"]["properties"]["studied_compartment"]["items"]["anyOf"][0]["enum"] = controlled_Voc['context.studied_compartment']
    schema["properties"]["context"]["properties"]["studied_compartment"]["items"]["enum"] = controlled_Voc['context.studied_compartment']
    #schema["properties"]["responsible_organisation"]["items"]["anyOf"][0]["properties"]["name"]["enum"] = controlled_Voc['responsible_organisation.name']
    schema["properties"]["responsible_organisation"]["items"]["properties"]["name"]["enum"] = controlled_Voc['responsible_organisation.name']
    #schema["properties"]["responsible_organisation"]["items"]["anyOf"][0]["properties"]["unit_name"]["enum"] = controlled_Voc['responsible_organisation.unit_name']
    schema["properties"]["responsible_organisation"]["items"]["properties"]["unit_name"]["enum"] = controlled_Voc['responsible_organisation.unit_name']
    #schema["properties"]["keyword"]["items"]["anyOf"][0]["properties"]["controlled_vocabulary"]["properties"]["date"]["items"]["anyOf"][0]["properties"]["type"]["enum"] = controlled_Voc['keyword.controlled_vocabulary.date.type']
    schema["properties"]["keyword"]["items"]["properties"]["controlled_vocabulary"]["properties"]["date"]["items"]["properties"]["type"]["enum"] = controlled_Voc['keyword.controlled_vocabulary.date.type']
    #schema["properties"]["studied_factor"]["items"]["anyOf"][0]["properties"]["name"]["enum"] = controlled_Voc['studied_factor.name']
    schema["properties"]["studied_factor"]["items"]["properties"]["name"]["enum"] = controlled_Voc['studied_factor.name']
    #schema["properties"]["studied_factor"]["items"]["anyOf"][0]["properties"]["comparison_modality"]["enum"] = controlled_Voc['studied_factor.comparison_modality']
    schema["properties"]["studied_factor"]["items"]["properties"]["comparison_modality"]["enum"] = controlled_Voc['studied_factor.comparison_modality']
    #schema["properties"]["biological_material"]["items"]["anyOf"][0]["properties"]["genetic_level"]["enum"] = controlled_Voc['biological_material.genetic_level']
    schema["properties"]["biological_material"]["items"]["properties"]["genetic_level"]["enum"] = controlled_Voc['biological_material.genetic_level']
    #schema["properties"]["biological_material"]["items"]["anyOf"][0]["properties"]["taxonomy_vocabulary"]["properties"]["date"]["items"]["anyOf"][0]["properties"]["type"]["enum"] = controlled_Voc['biological_material.taxonomy_vocabulary.date.type']
    schema["properties"]["biological_material"]["items"]["properties"]["taxonomy_vocabulary"]["properties"]["date"]["items"]["properties"]["type"]["enum"] = controlled_Voc['biological_material.taxonomy_vocabulary.date.type']
    #schema["properties"]["variable"]["items"]["anyOf"][0]["properties"]["observable_property"]["enum"] = controlled_Voc['variable.observable_property']
    schema["properties"]["variable"]["items"]["properties"]["observable_property"]["enum"] = controlled_Voc['variable.observable_property']
    #schema["properties"]["variable"]["items"]["anyOf"][0]["properties"]["object_of_interest"]["enum"] = controlled_Voc['variable.object_of_interest']
    schema["properties"]["variable"]["items"]["properties"]["object_of_interest"]["enum"] = controlled_Voc['variable.object_of_interest']
    #schema["properties"]["variable"]["items"]["anyOf"][0]["properties"]["data"]["items"]["anyOf"][0]["properties"]["acquisition_mode"]["enum"] = controlled_Voc['variable.data.acquisition_mode']
    schema["properties"]["variable"]["items"]["properties"]["data"]["items"]["properties"]["acquisition_mode"]["enum"] = controlled_Voc['variable.data.acquisition_mode']
    #schema["properties"]["variable"]["items"]["anyOf"][0]["properties"]["data"]["items"]["anyOf"][0]["properties"]["information_granularity"]["enum"] = controlled_Voc['variable.data.information_granularity']
    schema["properties"]["variable"]["items"]["properties"]["data"]["items"]["properties"]["information_granularity"]["enum"] = controlled_Voc['variable.data.information_granularity']
    schema["properties"]["experimental_site"]["properties"]["stand_structure"]["enum"] = controlled_Voc['experimental_site.stand_structure']
    schema["properties"]["experimental_site"]["properties"]["experiment_status"]["enum"] = controlled_Voc['experimental_site.experiment_status']
    schema["properties"]["experimental_site"]["properties"]["experiment_design"]["enum"] = controlled_Voc['experimental_site.experiment_design']
    #schema["properties"]["experimental_site"]["properties"]["other_information_available"]["items"]["anyOf"][0]["enum"] = controlled_Voc['experimental_site.other_information_available']
    schema["properties"]["experimental_site"]["properties"]["other_information_available"]["items"]["enum"] = controlled_Voc['experimental_site.other_information_available']
    schema["properties"]["metadata"]["properties"]["language"]["enum"] = controlled_Voc['metadata.language']
    #schema["properties"]["metadata"]["properties"]["responsible_organisation"]["items"]["anyOf"][0]["properties"]["name"]["enum"] = controlled_Voc['metadata.responsible_organisation.name']
    schema["properties"]["metadata"]["properties"]["responsible_organisation"]["items"]["properties"]["name"]["enum"] = controlled_Voc['metadata.responsible_organisation.name']

else: # remove enumeration lists
    print("Vocabularies NOT checked")

if args.s:
    print("Synthesis file will be generated")
else:
    print("No synthesis required")

if args.ss:
    print("Output simplified files option activated")
else:
    print("No simplified output")

#load json records to validate
#f = open('/home/cpichot/Documents/Projets/France/IR/IN_SYLVA/realisation/SI/Portail/InitialisationPortail/2024/Schema/EpiceaSylviculture_InSylvaSearchResults_utf8.json')

#validation output
outputDir = args.output_directory
print("Validation results in:",outputDir)

error_file_path= os.path.join(outputDir,"error_log.txt")
with open(args.input_json, 'r', encoding=json_encoding) as json_file:
    try:
        data = json.load(json_file)
    except json.JSONDecodeError as e:
        with open(error_file_path, "w") as error_file:
            error_file.write(str(e))
        print(f"Erreur lors de la lecture du fichier JSON avec l'encodage {json_encoding}: {e}")
        sys.exit(1)


#nb of records
print(len(data['metadataRecords']), "record(s) to validate")



outputSynthesisFile = outputDir+"/validationSynthesis.txt"
print("Validation synthesis:", outputSynthesisFile)
validationDate = time.strftime("%a, %d %b %Y %H:%M:%S")

if args.s:
    with open(outputSynthesisFile, "a") as synthesisFile:
        synthesisFile.write("\n\n-------------------------------------")
        synthesisFile.write("\n\n----- " + validationDate + " -----")
        outputText = "\n-- Validation of "+ args.input_json + " using " + args.schema
        synthesisFile.write(outputText)
        synthesisFile.write("\n-------------------------------------\n")

validator = jsonschema.Draft7Validator(schema, format_checker=FormatChecker())

record_id = 0
#loop over records
for key in data['metadataRecords']:
  record = data['metadataRecords'][record_id]
  #print(record_id + 1, ":", len(record))
  if "forest_type" in record['context']:
    if not record['context']['forest_type'] or record['context']['forest_type'] == "":
        	del record['context']['forest_type']
    if "reference_soil_group" in record['context']:
    	if not record['context']['reference_soil_group'] or record['context']['reference_soil_group'] == "":
        	del record['context']['reference_soil_group']
    if "humus_type" in record['context']:
    	if not record['context']['humus_type'] or record['context']['humus_type'] == "":
        	del record['context']['humus_type']
    if "studied_compartment" in record['context']:
    	if not record['context']['studied_compartment'] or record['context']['studied_compartment'] == "":
        	del record['context']['studied_compartment']
    if "experiment_status" in record['experimental_site']:
    	if not record['experimental_site']['experiment_status'] or record['experimental_site']['experiment_status'] == "":
        	del record['experimental_site']['experiment_status']
    if "experiment_design" in record['experimental_site']:
    	if not record['experimental_site']['experiment_design'] or record['experimental_site']['experiment_design'] == "":
        	del record['experimental_site']['experiment_design']
    if "other_information_available" in record['experimental_site']:
    	if not record['experimental_site']['other_information_available'] or record['experimental_site']['other_information_available'] == "":
        	del record['experimental_site']['other_information_available']


    #validate
    errors = validator.iter_errors(record)  # get all validation errors    errorsbis = errors
    nb_errors = 0
    with open(outputDir + "/record_" + str(record_id + 1) + ".txt", "w") as exportFile:
        for error in errors:
            nb_errors = nb_errors + 1
            exportFile.write("\n-------------------------------------")
            exportFile.write("\n--- << NEXT ERROR >> ----------------")
            exportFile.write("\n-------   ||   ----------------------")
            exportFile.write("\n-------   \/   --------------------\n")
            exportFile.write("Validation failed for "+str(error.absolute_path)[5:])
            exportFile.write("\n")
            exportFile.write("==> " + str(error.message) + "\n")

            exportFile.write(str(error))
    if args.ss:
        output_simplifier.process_errors(outputDir + "/record_" + str(record_id + 1) + ".txt",outputDir + "/record_" + str(record_id + 1) + "_simplified.txt")

    #print("record",record_id + 1, ":", str(nb_errors) + " errors")
    if args.s: 
        with open(outputSynthesisFile, "a") as synthesisFile:
            outputText = "record" + str(record_id + 1) + ":" + str(nb_errors) + " errors\n"
            synthesisFile.write(outputText)

    record_id = record_id + 1


