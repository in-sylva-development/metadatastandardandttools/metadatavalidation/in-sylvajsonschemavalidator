import re

def parse_error_block(error_block):
    """Parse a block of error text and extract relevant information."""
    error_info = {}
    
    # Extract the existing value that caused the error
    existing_value_match = re.search(r"'(.+)' is not one of \[", error_block)
    if existing_value_match:
        error_info['existing_value'] = existing_value_match.group(1)
    
    # Extract the list of valid values (enumeration)
    enum_values_match = re.search(r"\[([^\]]+)\]", error_block)
    if enum_values_match:
        error_info['valid_values'] = enum_values_match.group(1).split(", ")
    
    # Extract the JSON path
    path_match = re.search(r"Failed validating 'enum' in schema\['properties'\](.+?):", error_block, re.DOTALL)
    if path_match:
        error_info['json_path'] = path_match.group(1).replace("']['", "/").replace("['", "").replace("']", "")
    
    # Extract the instance path
    instance_path_match = re.search(r"On instance\['(.+?)'\]:", error_block, re.DOTALL)
    if instance_path_match:
        error_info['instance_path'] = instance_path_match.group(1).replace("']['", "/")
    
    return error_info

def process_errors(input_file, output_file):
    with open(input_file, 'r') as f:
        lines = f.readlines()
    
    error_blocks = []
    current_block = []
    for line in lines:
        if line.strip() == '--- << NEXT ERROR >> ----------------':
            if current_block:
                error_blocks.append("\n".join(current_block))
                current_block = []
        current_block.append(line)
    if current_block:
        error_blocks.append("\n".join(current_block))
    
    with open(output_file, 'w') as f:
        for block in error_blocks:
            error_info = parse_error_block(block)
            if error_info:
                f.write(f"JSON Path: {error_info.get('json_path', 'N/A')}\n")
                f.write(f"Instance Path: {error_info.get('instance_path', 'N/A')}\n")
                f.write(f"Existing Value: {error_info.get('existing_value', 'N/A')}\n")
                f.write(f"Reason: Value not in the allowed enumeration\n")
                f.write(f"Allowed Values: {', '.join(error_info.get('valid_values', []))}\n")
                f.write("\n")

